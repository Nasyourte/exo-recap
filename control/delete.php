<?php

if ($_SERVER['REQUEST_METHOD'] === 'DELETE'){
    include('pdo.php');
    global $pdo;
    $pdo->query("SET FOREIGN_KEY_CHECKS = 0;");
    $pdo->query("TRUNCATE people;");
    $pdo->query("TRUNCATE contact;");
    $pdo->query("SET FOREIGN_KEY_CHECKS = 1;");
    exec("node ../jsback/makejson.mjs 2>&1", $out, $err);
    echo "DELETED";
}else{
    echo "ERREUR";
}