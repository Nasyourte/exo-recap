<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = json_decode(file_get_contents('php://input'));
    try{
        include('pdo.php');
        global $pdo;
        $req = $pdo->prepare("INSERT INTO contact(person_one, person_two) VALUES (?, ?);");
        $req->execute([$data->p1, $data->p2]);
        exec("node ../jsback/makejson.mjs 2>&1", $out, $err);
        echo 'CONTACT OK';
    }
    catch (Exception $e) {
        http_response_code(400);
        echo 'ERREUR';
    }
}