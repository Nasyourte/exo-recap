<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $nom = json_decode(file_get_contents('php://input'))->nom;
    if( (strlen($nom) > 0) && (strlen($nom) <= 200)){
        include('pdo.php');
        global $pdo;
        $req = $pdo->prepare("INSERT INTO people(nom) VALUES (?);");
        $req->execute([$nom]);
        $id = $pdo->lastinsertid();
        exec("node ../jsback/makejson.mjs 2>&1", $out, $err);
        $python_cmd = "cd ../avatar && . bin/activate && python3 randy.py ".$id." 2>&1";
        exec($python_cmd, $out, $err);
        echo 'TOUT VA BIEN';
    }else{
        http_response_code(400);
        echo 'ERREUR';
    }
}
