drop database if exists contactapp;
create database contactapp;
use contactapp;

create table people(
    id int not null auto_increment primary key,
    nom varchar(200)
);

create table contact(
    person_one int,
    person_two int,
    foreign key (person_one) references people(id) on delete cascade,
    foreign key (person_two) references people(id) on delete cascade
);

grant all privileges on contactapp.* to 'tousStopAntiCovid'@'localhost';