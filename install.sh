#!/bin/bash
sudo mysql < data/init_contact.sql
sudo mysql < data/mocup.sql
cd control
node ../jsback/makejson.mjs
cd ../avatar
source bin/activate
counter=1
while [ $counter -le 50 ]
do
    touch $counter.png
    python3 randy.py $counter
    ((counter++))
done
echo All done
